package com.cds.attendance.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import com.cds.attendance.process.CalculateExtraHour;
import com.cds.attendance.process.ImportAttendance;
import com.cds.attendance.process.ImportAttendanceBioadmin;
import com.cds.attendance.process.ImportAttendanceFromAttachment;
import com.cds.attendance.process.ImportAttendanceFromAttachmentBioadmin;
import com.cds.attendance.process.ImportAttendanceFromAttachmentITAS;
import com.cds.attendance.process.ImportAttendanceFromAttachmentzk;
import com.cds.attendance.process.ImportAttendanceFromServerREST;
import com.cds.attendance.process.ProcessAttendance;
import com.cds.attendance.process.ProcessAttendanceBioadmin;
import com.cds.attendance.process.ImportAttendanceBioadminClkCode;

public class ProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		
		if(className.equals("com.cds.attendance.process.ImportAttendance"))
			return new ImportAttendance();		
		if(className.equals("com.cds.attendance.process.ImportAttendanceFromAttachment"))
			return new ImportAttendanceFromAttachment();
		if(className.equals("com.cds.attendance.process.ProcessAttendance"))
			return new ProcessAttendance();
		if(className.equals("com.cds.attendance.process.CalculateExtraHour"))
			return new CalculateExtraHour();
		if(className.equals("com.cds.attendance.process.ImportAttendanceFromAttachmentzk"))
			return new ImportAttendanceFromAttachmentzk();
		if(className.equals("com.cds.attendance.process.ImportAttendanceFromAttachmentBioadmin"))
			return new ImportAttendanceFromAttachmentBioadmin();
		if(className.equals("com.cds.attendance.process.ImportAttendanceBioadmin"))
			return new ImportAttendanceBioadmin();
		if(className.equals("com.cds.attendance.process.ImportAttendanceFromAttachmentITAS"))
			return new ImportAttendanceFromAttachmentITAS();
		if(className.equals("com.cds.attendance.process.ProcessAttendanceBioadmin"))
			return new ProcessAttendanceBioadmin();
		if(className.equals("com.cds.attendance.process.ImportAttendanceBioadminClkCode"))
			return new ImportAttendanceBioadminClkCode();
			if(className.equals("com.cds.attendance.process.ImportAttendanceFromServerREST"))
			return new ImportAttendanceFromServerREST();
		return null;
	}

}
