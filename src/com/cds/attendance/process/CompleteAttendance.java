package com.cds.attendance.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import org.compiere.model.MRefList;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.cds.attendance.base.model.MGH_Shifts;
import com.cds.attendance.base.model.MGH_ShiftsLine;
import com.cds.attendance.base.model.MHR_Attendance;
import com.cds.attendance.base.model.MMarking;

public class CompleteAttendance extends SvrProcess{

	private int p_GH_Shifts_ID;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] parameters = getParameter();
		for (ProcessInfoParameter para: parameters)
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("Description"))
				p_GH_Shifts_ID = para.getParameterAsInt();
			
		}
		
		
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		List<MMarking> listattendance = new Query(getCtx(), MMarking.Table_Name, "time2 is null", get_TrxName()).list();
		for(MMarking attendance : listattendance) {
			
			getLostTime(p_GH_Shifts_ID,attendance);			
		}
		
		return null;
	}

	protected void getLostTime(int p_GH_Shifts_ID2, MMarking attendance) {
		// TODO Auto-generated method stub
		MGH_Shifts shift = new MGH_Shifts(getCtx(), p_GH_Shifts_ID2, get_TrxName());
		String WeekDay = attendance.getWeekDayValue();
		if (WeekDay.equals(null))
			return;
		
		MGH_ShiftsLine shiftsLine = new Query(getCtx(), MGH_ShiftsLine.Table_Name, "GH_Shift_ID=? AND WeekDay=?", get_TrxName()).setParameters(p_GH_Shifts_ID2,WeekDay).first();
		long DifTime1 = attendance.getTime1().getTime() - shiftsLine.getTime1().getTime();
		long DifTime2 = attendance.getTime1().getTime() - shiftsLine.getTime2().getTime();
		long DifTime3 = attendance.getTime1().getTime() - shiftsLine.getTime3().getTime();
		long DifTime4 = attendance.getTime1().getTime() - shiftsLine.getTime4().getTime();
		int min = 0;
		if(DifTime1<DifTime2)
			min = 1;
		else 
			min =2;
		
		if(DifTime2<DifTime3)
			min = 1;
		else
			min =2;
		
	}

}
